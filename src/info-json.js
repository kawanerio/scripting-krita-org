const AvailableActions = [
  {
    id: "save_incremental_version",
    description: "Save Incremental &Version",
    tooltip: "Save Incremental Version",
  },
  {
    id: "save_incremental_backup",
    description: "Save Incremental &Backup",
    tooltip: "Save Incremental Backup",
  },
  {
    id: "tablet_debugger",
    description: "Toggle Tablet Debugger",
    tooltip: "Toggle Tablet Debugger",
  },
  {
    id: "create_template",
    description: "&Create Template From Image...",
    tooltip: "Create Template From Image",
  },
  {
    id: "create_copy",
    description: "Create Copy &From Current Image",
    tooltip: "Create Copy From Current Image",
  },
  {
    id: "open_resources_directory",
    description: "Open Resources Folder",
    tooltip:
      "Opens a file browser at the location Krita saves resources such as brushes to.",
  },
  {
    id: "rotate_canvas_right",
    description: "Rotate &Canvas Right",
    tooltip: "Rotate Canvas Right",
  },
  {
    id: "rotate_canvas_left",
    description: "Rotate Canvas &Left",
    tooltip: "Rotate Canvas Left",
  },
  {
    id: "reset_canvas_rotation",
    description: "R&eset Canvas Rotation",
    tooltip: "Reset Canvas Rotation",
  },
  {
    id: "wrap_around_mode",
    description: "&Wrap Around Mode",
    tooltip: "Wrap Around Mode",
  },
  {
    id: "level_of_detail_mode",
    description: "&Instant Preview Mode",
    tooltip: "Instant Preview Mode",
  },
  {
    id: "softProof",
    description: "Soft &Proofing",
    tooltip: "Turns on Soft Proofing",
  },
  {
    id: "gamutCheck",
    description: "&Out of Gamut Warnings",
    tooltip:
      "Turns on warnings for colors out of proofed gamut, needs soft proofing to be turned on.",
  },
  {
    id: "showStatusBar",
    description: "Show Status &Bar",
    tooltip: "Show or hide the status bar",
  },
  {
    id: "view_show_canvas_only",
    description: "&Show Canvas Only",
    tooltip: "Show just the canvas or the whole window",
  },
  {
    id: "ruler_pixel_multiple2",
    description: "Use multiple of 2 for pixel scale",
    tooltip: "Use multiple of 2 for pixel scale",
  },
  {
    id: "view_ruler",
    description: "Show &Rulers",
    tooltip: "Show Rulers",
  },
  {
    id: "rulers_track_mouse",
    description: "Rul&ers Track Pointer",
    tooltip: "Rulers Track Pointer",
  },
  {
    id: "zoom_to_100pct",
    description: "&Reset zoom",
    tooltip: "Reset zoom",
  },
  {
    id: "view_zoom_in",
    description: "Zoom &In",
    tooltip: "Zoom In",
  },
  {
    id: "view_zoom_out",
    description: "Zoom &Out",
    tooltip: "Zoom Out",
  },
  {
    id: "settings_active_author",
    description: "&Active Author Profile",
    tooltip: "Active Author Profile",
  },
  {
    id: "view_pixel_grid",
    description: "Show Pi&xel Grid",
    tooltip: "Show Pixel Grid",
  },
  {
    id: "toggle_fg_bg",
    description: "Swap Foreground and Background Color",
    tooltip: "Swap Foreground and Background Color",
  },
  {
    id: "reset_fg_bg",
    description: "Reset Foreground and Background Color",
    tooltip: "Reset Foreground and Background Color",
  },
  {
    id: "filter_apply_again",
    description: "&Apply Filter Again",
    tooltip: "Apply Filter Again",
  },
  {
    id: "adjust_filters",
    description: "A&djust",
    tooltip: "Adjust",
  },
  {
    id: "krita_filter_asc-cdl",
    description: "&Slope, Offset, Power...",
    tooltip: "Slope, Offset, Power",
  },
  {
    id: "krita_filter_autocontrast",
    description: "&Auto Contrast",
    tooltip: "Auto Contrast",
  },
  {
    id: "blur_filters",
    description: "&Blur",
    tooltip: "Blur",
  },
  {
    id: "krita_filter_blur",
    description: "&Blur...",
    tooltip: "Blur",
  },
  {
    id: "krita_filter_burn",
    description: "&Burn...",
    tooltip: "Burn",
  },
  {
    id: "krita_filter_colorbalance",
    description: "&Color Balance...",
    tooltip: "Color Balance",
  },
  {
    id: "color_filters",
    description: "&Colors",
    tooltip: "Colors",
  },
  {
    id: "krita_filter_colortoalpha",
    description: "&Color to Alpha...",
    tooltip: "Color to Alpha",
  },
  {
    id: "krita_filter_colortransfer",
    description: "Color &Transfer...",
    tooltip: "Color Transfer",
  },
  {
    id: "krita_filter_crosschannel",
    description: "C&ross-channel adjustment curves...",
    tooltip: "Cross-channel adjustment curves",
  },
  {
    id: "krita_filter_desaturate",
    description: "&Desaturate...",
    tooltip: "Desaturate",
  },
  {
    id: "krita_filter_dodge",
    description: "D&odge...",
    tooltip: "Dodge",
  },
  {
    id: "edge_filters",
    description: "&Edge Detection",
    tooltip: "Edge Detection",
  },
  {
    id: "krita_filter_edge detection",
    description: "&Edge Detection...",
    tooltip: "Edge Detection",
  },
  {
    id: "emboss_filters",
    description: "Emboss",
    tooltip: "Emboss",
  },
  {
    id: "krita_filter_emboss",
    description: "&Emboss with Variable Depth...",
    tooltip: "Emboss with Variable Depth",
  },
  {
    id: "krita_filter_emboss all directions",
    description: "Emboss &in All Directions",
    tooltip: "Emboss in All Directions",
  },
  {
    id: "krita_filter_emboss horizontal and vertical",
    description: "Emboss &Horizontal && Vertical",
    tooltip: "Emboss Horizontal & Vertical",
  },
  {
    id: "krita_filter_emboss horizontal only",
    description: "Emboss Horizontal &Only",
    tooltip: "Emboss Horizontal Only",
  },
  {
    id: "krita_filter_emboss laplascian",
    description: "Emboss (&Laplacian)",
    tooltip: "Emboss (Laplacian)",
  },
  {
    id: "krita_filter_emboss vertical only",
    description: "Emboss &Vertical Only",
    tooltip: "Emboss Vertical Only",
  },
  {
    id: "krita_filter_gaussian blur",
    description: "&Gaussian Blur...",
    tooltip: "Gaussian Blur",
  },
  {
    id: "krita_filter_gaussianhighpass",
    description: "&Gaussian High Pass...",
    tooltip: "Gaussian High Pass",
  },
  {
    id: "enhance_filters",
    description: "E&nhance",
    tooltip: "Enhance",
  },
  {
    id: "krita_filter_gaussiannoisereducer",
    description: "&Gaussian Noise Reduction...",
    tooltip: "Gaussian Noise Reduction",
  },
  {
    id: "map_filters",
    description: "&Map",
    tooltip: "Map",
  },
  {
    id: "krita_filter_gradientmap",
    description: "&Gradient Map...",
    tooltip: "Gradient Map",
  },
  {
    id: "artistic_filters",
    description: "Ar&tistic",
    tooltip: "Artistic",
  },
  {
    id: "krita_filter_halftone",
    description: "&Halftone...",
    tooltip: "Halftone",
  },
  {
    id: "krita_filter_height to normal",
    description: "&Height to Normal Map...",
    tooltip: "Height to Normal Map",
  },
  {
    id: "krita_filter_hsvadjustment",
    description: "&HSV Adjustment...",
    tooltip: "HSV Adjustment",
  },
  {
    id: "krita_filter_indexcolors",
    description: "&Index Colors...",
    tooltip: "Index Colors",
  },
  {
    id: "krita_filter_invert",
    description: "&Invert",
    tooltip: "Invert",
  },
  {
    id: "krita_filter_lens blur",
    description: "&Lens Blur...",
    tooltip: "Lens Blur",
  },
  {
    id: "krita_filter_levels",
    description: "&Levels...",
    tooltip: "Levels",
  },
  {
    id: "krita_filter_maximize",
    description: "M&aximize Channel",
    tooltip: "Maximize Channel",
  },
  {
    id: "krita_filter_mean removal",
    description: "&Mean Removal",
    tooltip: "Mean Removal",
  },
  {
    id: "krita_filter_minimize",
    description: "M&inimize Channel",
    tooltip: "Minimize Channel",
  },
  {
    id: "krita_filter_motion blur",
    description: "&Motion Blur...",
    tooltip: "Motion Blur",
  },
  {
    id: "other_filters",
    description: "&Other",
    tooltip: "Other",
  },
  {
    id: "krita_filter_noise",
    description: "&Random Noise...",
    tooltip: "Random Noise",
  },
  {
    id: "krita_filter_normalize",
    description: "&Normalize",
    tooltip: "Normalize",
  },
  {
    id: "krita_filter_oilpaint",
    description: "&Oilpaint...",
    tooltip: "Oilpaint",
  },
  {
    id: "krita_filter_palettize",
    description: "&Palettize...",
    tooltip: "Palettize",
  },
  {
    id: "krita_filter_perchannel",
    description: "Color Ad&justment curves...",
    tooltip: "Color Adjustment curves",
  },
  {
    id: "krita_filter_phongbumpmap",
    description: "Phong &Bumpmap...",
    tooltip: "Phong Bumpmap",
  },
  {
    id: "krita_filter_pixelize",
    description: "&Pixelize...",
    tooltip: "Pixelize",
  },
  {
    id: "krita_filter_posterize",
    description: "Po&sterize...",
    tooltip: "Posterize",
  },
  {
    id: "krita_filter_raindrops",
    description: "&Raindrops...",
    tooltip: "Raindrops",
  },
  {
    id: "krita_filter_randompick",
    description: "Random &Pick...",
    tooltip: "Random Pick",
  },
  {
    id: "krita_filter_roundcorners",
    description: "&Round Corners...",
    tooltip: "Round Corners",
  },
  {
    id: "krita_filter_sharpen",
    description: "&Sharpen",
    tooltip: "Sharpen",
  },
  {
    id: "krita_filter_smalltiles",
    description: "&Small Tiles...",
    tooltip: "Small Tiles",
  },
  {
    id: "krita_filter_threshold",
    description: "&Threshold...",
    tooltip: "Threshold",
  },
  {
    id: "krita_filter_unsharp",
    description: "&Unsharp Mask...",
    tooltip: "Unsharp Mask",
  },
  {
    id: "krita_filter_wave",
    description: "&Wave...",
    tooltip: "Wave",
  },
  {
    id: "krita_filter_waveletnoisereducer",
    description: "&Wavelet Noise Reducer...",
    tooltip: "Wavelet Noise Reducer",
  },
  {
    id: "edit_cut",
    description: "Cu&t",
    tooltip: "Cut selection to clipboard",
  },
  {
    id: "edit_copy",
    description: "&Copy",
    tooltip: "Copy selection to clipboard",
  },
  {
    id: "edit_paste",
    description: "&Paste",
    tooltip: "Paste clipboard content",
  },
  {
    id: "copy_sharp",
    description: "C&opy (sharp)",
    tooltip: "Copy (sharp)",
  },
  {
    id: "cut_sharp",
    description: "Cut (&sharp)",
    tooltip: "Cut (sharp)",
  },
  {
    id: "paste_new",
    description: "Paste into &New Image",
    tooltip: "Paste into New Image",
  },
  {
    id: "paste_at",
    description: "Paste &at Cursor",
    tooltip: "Paste at cursor",
  },
  {
    id: "paste_as_reference",
    description: "Paste as R&eference Image",
    tooltip: "Paste as Reference Image",
  },
  {
    id: "copy_merged",
    description: "Copy &merged",
    tooltip: "Copy merged",
  },
  {
    id: "select_all",
    description: "Select &All",
    tooltip: "Select All",
  },
  {
    id: "deselect",
    description: "&Deselect",
    tooltip: "Deselect",
  },
  {
    id: "clear",
    description: "C&lear",
    tooltip: "Clear",
  },
  {
    id: "reselect",
    description: "&Reselect",
    tooltip: "Reselect",
  },
  {
    id: "invert_selection",
    description: "&Invert Selection",
    tooltip: "Invert current selection",
  },
  {
    id: "copy_selection_to_new_layer",
    description: "Copy &Selection to New Layer",
    tooltip: "Copy Selection to New Layer",
  },
  {
    id: "cut_selection_to_new_layer",
    description: "Cut Selection &to New Layer",
    tooltip: "Cut Selection to New Layer",
  },
  {
    id: "fill_selection_foreground_color",
    description: "&Fill with Foreground Color",
    tooltip: "Fill with Foreground Color",
  },
  {
    id: "fill_selection_background_color",
    description: "Fill &with Background Color",
    tooltip: "Fill with Background Color",
  },
  {
    id: "fill_selection_pattern",
    description: "F&ill with Pattern",
    tooltip: "Fill with Pattern",
  },
  {
    id: "fill_selection_foreground_color_opacity",
    description: "&Fill with Foreground Color (Opacity)",
    tooltip: "Fill with Foreground Color (Opacity)",
  },
  {
    id: "fill_selection_background_color_opacity",
    description: "Fill &with Background Color (Opacity)",
    tooltip: "Fill with Background Color (Opacity)",
  },
  {
    id: "fill_selection_pattern_opacity",
    description: "Fill with &Pattern (Opacity)",
    tooltip: "Fill with Pattern (Opacity)",
  },
  {
    id: "stroke_shapes",
    description: "Stro&ke selected shapes",
    tooltip: "Stroke selected shapes",
  },
  {
    id: "toggle_display_selection",
    description: "Dis&play Selection",
    tooltip: "Display Selection",
  },
  {
    id: "resizeimagetoselection",
    description: "Trim to S&election",
    tooltip: "Trim to Selection",
  },
  {
    id: "edit_selection",
    description: "Edit Selection",
    tooltip: "Edit Selection",
  },
  {
    id: "convert_to_vector_selection",
    description: "&Convert to Vector Selection",
    tooltip: "Convert to Vector Selection",
  },
  {
    id: "convert_to_raster_selection",
    description: "Co&nvert to Raster Selection",
    tooltip: "Convert to Raster Selection",
  },
  {
    id: "convert_shapes_to_vector_selection",
    description: "Convert Shapes to &Vector Selection",
    tooltip: "Convert Shapes to Vector Selection",
  },
  {
    id: "convert_selection_to_shape",
    description: "Convert &to Shape",
    tooltip: "Convert to Shape",
  },
  {
    id: "toggle-selection-overlay-mode",
    description: "&Toggle Selection Display Mode",
    tooltip: "Toggle Selection Display Mode",
  },
  {
    id: "stroke_selection",
    description: "Stroke Selec&tion...",
    tooltip: "Stroke selection",
  },
  {
    id: "view_show_guides",
    description: "Show Guides",
    tooltip: "Show or hide guides",
  },
  {
    id: "view_lock_guides",
    description: "&Lock Guides",
    tooltip: "Lock or unlock guides",
  },
  {
    id: "view_snap_to_guides",
    description: "&Snap to Guides",
    tooltip: "Snap cursor to guides position",
  },
  {
    id: "show_snap_options_popup",
    description: "Show Snap Options Popup",
    tooltip: "Show Snap Options Popup",
  },
  {
    id: "view_snap_orthogonal",
    description: "Snap &Orthogonal",
    tooltip: "Snap Orthogonal",
  },
  {
    id: "view_snap_node",
    description: "Snap &Node",
    tooltip: "Snap Node",
  },
  {
    id: "view_snap_extension",
    description: "Snap &Extension",
    tooltip: "Snap Extension",
  },
  {
    id: "view_snap_intersection",
    description: "Snap &Intersection",
    tooltip: "Snap Intersection",
  },
  {
    id: "view_snap_bounding_box",
    description: "Snap &Bounding Box",
    tooltip: "Snap Bounding Box",
  },
  {
    id: "view_snap_image_bounds",
    description: "Sn&ap Image Bounds",
    tooltip: "Snap Image Bounds",
  },
  {
    id: "view_snap_image_center",
    description: "Snap Image &Center",
    tooltip: "Snap Image Center",
  },
  {
    id: "view_snap_to_pixel",
    description: "Snap &Pixel",
    tooltip: "Snap Pixel",
  },
  {
    id: "flatten_image",
    description: "Flatten ima&ge",
    tooltip: "Flatten image",
  },
  {
    id: "merge_layer",
    description: "Merge with Layer Below",
    tooltip: "Merge with Layer Below",
  },
  {
    id: "flatten_layer",
    description: "&Flatten Layer",
    tooltip: "Flatten Layer",
  },
  {
    id: "rasterize_layer",
    description: "&Rasterize Layer",
    tooltip: "Rasterize Layer",
  },
  {
    id: "save_groups_as_images",
    description: "Save &Group Layers...",
    tooltip: "Save Group Layers",
  },
  {
    id: "convert_group_to_animated",
    description: "Convert group to &animated layer",
    tooltip: "Convert child layers into animation frames",
  },
  {
    id: "resizeimagetolayer",
    description: "Trim to Current &Layer",
    tooltip: "Trim to Current Layer",
  },
  {
    id: "trim_to_image",
    description: "&Trim to Image Size",
    tooltip: "Trim to Image Size",
  },
  {
    id: "layer_style",
    description: "La&yer Style...",
    tooltip: "Layer Style",
  },
  {
    id: "mirrorNodeX",
    description: "Mirror Layer Hori&zontally",
    tooltip: "Mirror Layer Horizontally",
  },
  {
    id: "mirrorNodeY",
    description: "Mirror Layer &Vertically",
    tooltip: "Mirror Layer Vertically",
  },
  {
    id: "mirrorAllNodesX",
    description: "Mirror All Layers Hori&zontally",
    tooltip: "Mirror All Layers Horizontally",
  },
  {
    id: "mirrorAllNodesY",
    description: "Mirror All Layers &Vertically",
    tooltip: "Mirror All Layers Vertically",
  },
  {
    id: "activateNextLayer",
    description: "Activate next layer",
    tooltip: "Activate next layer",
  },
  {
    id: "activateNextSiblingLayer",
    description: "Activate next sibling layer, skipping over groups.",
    tooltip: "Activate next sibling layer",
  },
  {
    id: "activatePreviousLayer",
    description: "Activate previous layer",
    tooltip: "Activate previous layer",
  },
  {
    id: "activatePreviousSiblingLayer",
    description: "Activate previous sibling layer, skipping over groups.",
    tooltip: "Activate previous sibling layer",
  },
  {
    id: "switchToPreviouslyActiveNode",
    description: "Activate previously selected layer",
    tooltip: "Activate previously selected layer",
  },
  {
    id: "save_node_as_image",
    description: "&Save Layer/Mask...",
    tooltip: "Save Layer/Mask",
  },
  {
    id: "save_vector_node_to_svg",
    description: "Save &Vector Layer as SVG...",
    tooltip: "Save Vector Layer as SVG",
  },
  {
    id: "duplicatelayer",
    description: "&Duplicate Layer or Mask",
    tooltip: "Duplicate Layer or Mask",
  },
  {
    id: "copy_layer_clipboard",
    description: "C&opy Layer",
    tooltip: "Copy layer to clipboard",
  },
  {
    id: "cut_layer_clipboard",
    description: "Cut &Layer",
    tooltip: "Cut layer to clipboard",
  },
  {
    id: "paste_layer_from_clipboard",
    description: "Paste Layer",
    tooltip: "Paste layer from clipboard",
  },
  {
    id: "create_quick_group",
    description: "&Quick Group",
    tooltip: "Create a group layer containing selected layers",
  },
  {
    id: "create_quick_clipping_group",
    description: "Quick &Clipping Group",
    tooltip: "Group selected layers and add a layer with clipped alpha channel",
  },
  {
    id: "quick_ungroup",
    description: "Quick &Ungroup",
    tooltip:
      "Remove grouping of the layers or remove one layer out of the group",
  },
  {
    id: "select_all_layers",
    description: "&All Layers",
    tooltip: "Select all layers",
  },
  {
    id: "select_visible_layers",
    description: "&Visible Layers",
    tooltip: "Select all visible layers",
  },
  {
    id: "select_locked_layers",
    description: "&Locked Layers",
    tooltip: "Select all locked layers",
  },
  {
    id: "select_invisible_layers",
    description: "&Invisible Layers",
    tooltip: "Select all invisible layers",
  },
  {
    id: "select_unlocked_layers",
    description: "&Unlocked Layers",
    tooltip: "Select all unlocked layers",
  },
  {
    id: "new_from_visible",
    description: "&New Layer From Visible",
    tooltip: "New layer from visible",
  },
  {
    id: "pin_to_timeline",
    description: "Pin to Timeline",
    tooltip:
      "If checked, layer becomes pinned to the timeline, making it visible even when inactive.",
  },
  {
    id: "add_new_paint_layer",
    description: "&Paint Layer",
    tooltip: "Paint Layer",
  },
  {
    id: "add_new_group_layer",
    description: "&Group Layer",
    tooltip: "Group Layer",
  },
  {
    id: "add_new_clone_layer",
    description: "&Clone Layer",
    tooltip: "Clone Layer",
  },
  {
    id: "add_new_shape_layer",
    description: "&Vector Layer",
    tooltip: "Vector Layer",
  },
  {
    id: "add_new_adjustment_layer",
    description: "&Filter Layer...",
    tooltip: "Filter Layer",
  },
  {
    id: "add_new_fill_layer",
    description: "&Fill Layer...",
    tooltip: "Fill Layer",
  },
  {
    id: "add_new_file_layer",
    description: "&File Layer...",
    tooltip: "File Layer",
  },
  {
    id: "add_new_transparency_mask",
    description: "&Transparency Mask",
    tooltip: "Transparency Mask",
  },
  {
    id: "add_new_filter_mask",
    description: "&Filter Mask...",
    tooltip: "Filter Mask",
  },
  {
    id: "add_new_colorize_mask",
    description: "&Colorize Mask",
    tooltip: "Colorize Mask",
  },
  {
    id: "add_new_transform_mask",
    description: "&Transform Mask...",
    tooltip: "Transform Mask",
  },
  {
    id: "add_new_selection_mask",
    description: "&Local Selection",
    tooltip: "Local Selection",
  },
  {
    id: "convert_to_paint_layer",
    description: "to &Paint Layer",
    tooltip: "to Paint Layer",
  },
  {
    id: "convert_to_selection_mask",
    description: "to &Selection Mask",
    tooltip: "to Selection Mask",
  },
  {
    id: "convert_to_filter_mask",
    description: "to &Filter Mask...",
    tooltip: "to Filter Mask",
  },
  {
    id: "convert_to_transparency_mask",
    description: "to &Transparency Mask",
    tooltip: "to Transparency Mask",
  },
  {
    id: "convert_to_animated",
    description: "Convert to &animated layer",
    tooltip: "Convert layer into animation frames",
  },
  {
    id: "convert_to_file_layer",
    description: "to File &Layer",
    tooltip:
      "Saves out the layers into a new image and then references that image.",
  },
  {
    id: "isolate_active_layer",
    description: "&Isolate Active Layer",
    tooltip: "Isolate Active Layer",
  },
  {
    id: "isolate_active_group",
    description: "&Isolate Active Group",
    tooltip: "Isolate Active Group",
  },
  {
    id: "toggle_layer_visibility",
    description: "Toggle layer &visibility",
    tooltip: "Toggle layer visibility",
  },
  {
    id: "toggle_layer_lock",
    description: "&Toggle layer lock",
    tooltip: "Toggle layer lock",
  },
  {
    id: "toggle_layer_inherit_alpha",
    description: "Toggle layer alpha &inheritance",
    tooltip: "Toggle layer alpha inheritance",
  },
  {
    id: "toggle_layer_alpha_lock",
    description: "Toggle layer &alpha",
    tooltip: "Toggle layer alpha",
  },
  {
    id: "split_alpha_into_mask",
    description: "&Alpha into Mask",
    tooltip: "Alpha into Mask",
  },
  {
    id: "split_alpha_write",
    description: "&Write as Alpha",
    tooltip: "Write as Alpha",
  },
  {
    id: "split_alpha_save_merged",
    description: "&Save Merged...",
    tooltip: "Save Merged",
  },
  {
    id: "import_layer_from_file",
    description: "I&mport Layer...",
    tooltip: "Import Layer",
  },
  {
    id: "image_properties",
    description: "&Properties...",
    tooltip: "Properties",
  },
  {
    id: "import_layer_as_paint_layer",
    description: "&as Paint Layer...",
    tooltip: "as Paint Layer",
  },
  {
    id: "import_layer_as_transparency_mask",
    description: "as &Transparency Mask...",
    tooltip: "as Transparency Mask",
  },
  {
    id: "import_layer_as_filter_mask",
    description: "as &Filter Mask...",
    tooltip: "as Filter Mask",
  },
  {
    id: "import_layer_as_selection_mask",
    description: "as &Selection Mask...",
    tooltip: "as Selection Mask",
  },
  {
    id: "image_color",
    description: "&Image Background Color and Transparency...",
    tooltip: "Change the background color of the image",
  },
  {
    id: "view_grid",
    description: "Show &Grid",
    tooltip: "Show Grid",
  },
  {
    id: "view_snap_to_grid",
    description: "Snap &To Grid",
    tooltip: "Snap To Grid",
  },
  {
    id: "view_toggle_painting_assistants",
    description: "S&how Painting Assistants",
    tooltip: "Show Painting Assistants",
  },
  {
    id: "view_toggle_assistant_previews",
    description: "Show &Assistant Previews",
    tooltip: "Show Assistant Previews",
  },
  {
    id: "view_toggle_reference_images",
    description: "Show Re&ference Images",
    tooltip: "Show Reference Images",
  },
  {
    id: "make_brush_color_lighter",
    description: "Make brush color lighter",
    tooltip: "Make brush color lighter",
  },
  {
    id: "make_brush_color_darker",
    description: "Make brush color darker",
    tooltip: "Make brush color darker",
  },
  {
    id: "make_brush_color_saturated",
    description: "Make brush color more saturated",
    tooltip: "Make brush color more saturated",
  },
  {
    id: "make_brush_color_desaturated",
    description: "Make brush color more desaturated",
    tooltip: "Make brush color more desaturated",
  },
  {
    id: "shift_brush_color_clockwise",
    description: "Shift brush color hue clockwise",
    tooltip: "Shift brush color hue clockwise",
  },
  {
    id: "shift_brush_color_counter_clockwise",
    description: "Shift brush color hue counter-clockwise",
    tooltip: "Shift brush color hue counter-clockwise",
  },
  {
    id: "make_brush_color_redder",
    description: "Make brush color more red",
    tooltip: "Make brush color more red",
  },
  {
    id: "make_brush_color_greener",
    description: "Make brush color more green",
    tooltip: "Make brush color more green",
  },
  {
    id: "make_brush_color_bluer",
    description: "Make brush color more blue",
    tooltip: "Make brush color more blue",
  },
  {
    id: "make_brush_color_yellower",
    description: "Make brush color more yellow",
    tooltip: "Make brush color more yellow",
  },
  {
    id: "increase_opacity",
    description: "Increase opacity",
    tooltip: "Increase opacity",
  },
  {
    id: "decrease_opacity",
    description: "Decrease opacity",
    tooltip: "Decrease opacity",
  },
  {
    id: "mirror_canvas",
    description: "&Mirror View",
    tooltip: "Mirror View",
  },
  {
    id: "patterns",
    description: "&Patterns",
    tooltip: "Patterns",
  },
  {
    id: "gradients",
    description: "&Gradients",
    tooltip: "Gradients",
  },
  {
    id: "dual",
    description: "&Color",
    tooltip: "Color",
  },
  {
    id: "erase_action",
    description: "Set eraser mode",
    tooltip: "Set eraser mode",
  },
  {
    id: "reload_preset_action",
    description: "Reload Original Preset",
    tooltip: "Reload Original Preset",
  },
  {
    id: "preserve_alpha",
    description: "Preserve Alpha",
    tooltip: "Preserve Alpha",
  },
  {
    id: "mirrorX-hideDecorations",
    description: "Hide Mirror X Line",
    tooltip: "Hide Mirror X Line",
  },
  {
    id: "mirrorX-lock",
    description: "Lock",
    tooltip: "Lock X Line",
  },
  {
    id: "mirrorX-moveToCenter",
    description: "Move to Canvas Center X",
    tooltip: "Move to Canvas Center X",
  },
  {
    id: "mirrorY-hideDecorations",
    description: "Hide Mirror Y Line",
    tooltip: "Hide Mirror Y Line",
  },
  {
    id: "mirrorY-lock",
    description: "Lock Y Line",
    tooltip: "Lock Y Line",
  },
  {
    id: "mirrorY-moveToCenter",
    description: "Move to Canvas Center Y",
    tooltip: "Move to Canvas Center Y",
  },
  {
    id: "hmirror_action",
    description: "Horizontal Mirror Tool",
    tooltip: "Horizontal Mirror Tool",
  },
  {
    id: "vmirror_action",
    description: "Vertical Mirror Tool",
    tooltip: "Vertical Mirror Tool",
  },
  {
    id: "Select Normal Blending Mode",
    description: "Select Normal Blending Mode",
    tooltip: "Select Normal Blending Mode",
  },
  {
    id: "Select Dissolve Blending Mode",
    description: "Select Dissolve Blending Mode",
    tooltip: "Select Dissolve Blending Mode",
  },
  {
    id: "Select Behind Blending Mode",
    description: "Select Behind Blending Mode",
    tooltip: "Select Behind Blending Mode",
  },
  {
    id: "Select Clear Blending Mode",
    description: "Select Clear Blending Mode",
    tooltip: "Select Clear Blending Mode",
  },
  {
    id: "Select Darken Blending Mode",
    description: "Select Darken Blending Mode",
    tooltip: "Select Darken Blending Mode",
  },
  {
    id: "Select Multiply Blending Mode",
    description: "Select Multiply Blending Mode",
    tooltip: "Select Multiply Blending Mode",
  },
  {
    id: "Select Color Burn Blending Mode",
    description: "Select Color Burn Blending Mode",
    tooltip: "Select Color Burn Blending Mode",
  },
  {
    id: "Select Linear Burn Blending Mode",
    description: "Select Linear Burn Blending Mode",
    tooltip: "Select Linear Burn Blending Mode",
  },
  {
    id: "Select Lighten Blending Mode",
    description: "Select Lighten Blending Mode",
    tooltip: "Select Lighten Blending Mode",
  },
  {
    id: "Select Screen Blending Mode",
    description: "Select Screen Blending Mode",
    tooltip: "Select Screen Blending Mode",
  },
  {
    id: "Select Color Dodge Blending Mode",
    description: "Select Color Dodge Blending Mode",
    tooltip: "Select Color Dodge Blending Mode",
  },
  {
    id: "Select Linear Dodge Blending Mode",
    description: "Select Linear Dodge Blending Mode",
    tooltip: "Select Linear Dodge Blending Mode",
  },
  {
    id: "Select Overlay Blending Mode",
    description: "Select Overlay Blending Mode",
    tooltip: "Select Overlay Blending Mode",
  },
  {
    id: "Select Hard Overlay Blending Mode",
    description: "Select Hard Overlay Blending Mode",
    tooltip: "Select Hard Overlay Blending Mode",
  },
  {
    id: "Select Soft Light Blending Mode",
    description: "Select Soft Light Blending Mode",
    tooltip: "Select Soft Light Blending Mode",
  },
  {
    id: "Select Hard Light Blending Mode",
    description: "Select Hard Light Blending Mode",
    tooltip: "Select Hard Light Blending Mode",
  },
  {
    id: "Select Vivid Light Blending Mode",
    description: "Select Vivid Light Blending Mode",
    tooltip: "Select Vivid Light Blending Mode",
  },
  {
    id: "Select Linear Light Blending Mode",
    description: "Select Linear Light Blending Mode",
    tooltip: "Select Linear Light Blending Mode",
  },
  {
    id: "Select Pin Light Blending Mode",
    description: "Select Pin Light Blending Mode",
    tooltip: "Select Pin Light Blending Mode",
  },
  {
    id: "Select Hard Mix Blending Mode",
    description: "Select Hard Mix Blending Mode",
    tooltip: "Select Hard Mix Blending Mode",
  },
  {
    id: "Select Difference Blending Mode",
    description: "Select Difference Blending Mode",
    tooltip: "Select Difference Blending Mode",
  },
  {
    id: "Select Exclusion Blending Mode",
    description: "Select Exclusion Blending Mode",
    tooltip: "Select Exclusion Blending Mode",
  },
  {
    id: "Select Hue Blending Mode",
    description: "Select Hue Blending Mode",
    tooltip: "Select Hue Blending Mode",
  },
  {
    id: "Select Saturation Blending Mode",
    description: "Select Saturation Blending Mode",
    tooltip: "Select Saturation Blending Mode",
  },
  {
    id: "Select Color Blending Mode",
    description: "Select Color Blending Mode",
    tooltip: "Select Color Blending Mode",
  },
  {
    id: "Select Luminosity Blending Mode",
    description: "Select Luminosity Blending Mode",
    tooltip: "Select Luminosity Blending Mode",
  },
  {
    id: "composite_actions",
    description: "Brush composite",
    tooltip: "Brush composite",
  },
  {
    id: "brushslider1",
    description: "Brush option slider 1",
    tooltip: "Brush option slider 1",
  },
  {
    id: "brushslider2",
    description: "Brush option slider 2",
    tooltip: "Brush option slider 2",
  },
  {
    id: "brushslider3",
    description: "Brush option slider 3",
    tooltip: "Brush option slider 3",
  },
  {
    id: "next_favorite_preset",
    description: "Next Favourite Preset",
    tooltip: "Next Favourite Preset",
  },
  {
    id: "previous_favorite_preset",
    description: "Previous Favourite Preset",
    tooltip: "Previous Favourite Preset",
  },
  {
    id: "previous_preset",
    description: "Switch to Previous Preset",
    tooltip: "Switch to Previous Preset",
  },
  {
    id: "show_brush_editor",
    description: "Show Brush Editor",
    tooltip: "Show Brush Editor",
  },
  {
    id: "show_brush_presets",
    description: "Show Brush Presets",
    tooltip: "Show Brush Presets",
  },
  {
    id: "mirror_actions",
    description: "Mirror",
    tooltip: "Mirror",
  },
  {
    id: "workspaces",
    description: "Workspaces",
    tooltip: "Workspaces",
  },
  {
    id: "disable_pressure",
    description: "Use Pen Pressure",
    tooltip: "Use Pen Pressure",
  },
  {
    id: "paintops",
    description: "&Painter's Tools",
    tooltip: "Painter's Tools",
  },
  {
    id: "render_animation",
    description: "Render Anima&tion...",
    tooltip: "Render Animation to GIF, Image Sequence or Video",
  },
  {
    id: "render_animation_again",
    description: "&Render Animation Again",
    tooltip: "Render Animation Again",
  },
  {
    id: "buginfo",
    description: "&Show Krita log for bug reports.",
    tooltip: "Show Krita log for bug reports.",
  },
  {
    id: "sysinfo",
    description: "Show system &information for bug reports.",
    tooltip: "Show system information for bug reports.",
  },
  {
    id: "clones_array",
    description: "Clones &Array...",
    tooltip: "Clones Array",
  },
  {
    id: "colorrange",
    description: "S&elect from Color Range...",
    tooltip: "Select from Color Range",
  },
  {
    id: "selectopaque",
    description: "Select &Opaque (Replace)",
    tooltip: "Select Opaque",
  },
  {
    id: "selectopaque_add",
    description: "Select Opaque (&Add)",
    tooltip: "Select Opaque (Add)",
  },
  {
    id: "selectopaque_subtract",
    description: "Select Opaque (&Subtract)",
    tooltip: "Select Opaque (Subtract)",
  },
  {
    id: "selectopaque_intersect",
    description: "Select Opaque (&Intersect)",
    tooltip: "Select Opaque (Intersect)",
  },
  {
    id: "imagecolorspaceconversion",
    description: "&Convert Image Color Space...",
    tooltip: "Convert Image Color Space",
  },
  {
    id: "layercolorspaceconversion",
    description: "&Convert Layer Color Space...",
    tooltip: "Convert Layer Color Space",
  },
  {
    id: "dbexplorer",
    description: "&Explore Resources Cache Database...",
    tooltip: "Resources Cache Database",
  },
  {
    id: "imagesize",
    description: "Scale Image To &New Size...",
    tooltip: "Scale Image To New Size",
  },
  {
    id: "canvassize",
    description: "R&esize Canvas...",
    tooltip: "Resize Canvas",
  },
  {
    id: "layersize",
    description: "Scale &Layer to new Size...",
    tooltip: "Scale Layer to new Size",
  },
  {
    id: "scaleAllLayers",
    description: "Scale All &Layers to new Size...",
    tooltip: "Scale All Layers to new Size",
  },
  {
    id: "selectionscale",
    description: "Sca&le...",
    tooltip: "Scale",
  },
  {
    id: "imagesplit",
    description: "Im&age Split ",
    tooltip: "Image Split",
  },
  {
    id: "LayerGroupSwitcher/previous",
    description: "Move into previous group",
    tooltip: "Move into previous group",
  },
  {
    id: "LayerGroupSwitcher/next",
    description: "Move into next group",
    tooltip: "Move into next group",
  },
  {
    id: "layersplit",
    description: "&Split Layer...",
    tooltip: "Split Layer",
  },
  {
    id: "EditLayerMetaData",
    description: "&Edit metadata...",
    tooltip: "Edit metadata",
  },
  {
    id: "growselection",
    description: "&Grow Selection...",
    tooltip: "Grow Selection",
  },
  {
    id: "shrinkselection",
    description: "S&hrink Selection...",
    tooltip: "Shrink Selection",
  },
  {
    id: "borderselection",
    description: "&Border Selection...",
    tooltip: "Border Selection",
  },
  {
    id: "featherselection",
    description: "&Feather Selection...",
    tooltip: "Feather Selection",
  },
  {
    id: "smoothselection",
    description: "S&mooth",
    tooltip: "Smooth",
  },
  {
    id: "offsetimage",
    description: "&Offset Image...",
    tooltip: "Offset Image",
  },
  {
    id: "offsetlayer",
    description: "&Offset Layer...",
    tooltip: "Offset Layer",
  },
  {
    id: "QMic",
    description: "&Start G'MIC-Qt",
    tooltip: "Start G'Mic-Qt",
  },
  {
    id: "QMicAgain",
    description: "&Re-apply the last G'MIC filter",
    tooltip: "Apply the last G'Mic-Qt action again",
  },
  {
    id: "import_bundles",
    description: "Import Bundles...",
    tooltip: "Import Bundles",
  },
  {
    id: "import_brushes",
    description: "Import Brushes...",
    tooltip: "Import Brushes",
  },
  {
    id: "import_gradients",
    description: "Import Gradients...",
    tooltip: "Import Gradients",
  },
  {
    id: "import_palettes",
    description: "Import Palettes...",
    tooltip: "Import Palettes",
  },
  {
    id: "import_patterns",
    description: "Import Patterns...",
    tooltip: "Import Patterns",
  },
  {
    id: "import_presets",
    description: "Import Presets...",
    tooltip: "Import Presets",
  },
  {
    id: "import_workspaces",
    description: "Import Workspaces...",
    tooltip: "Import Workspaces",
  },
  {
    id: "create_bundle",
    description: "Create Resource Bundle...",
    tooltip: "Create Resource Bundle",
  },
  {
    id: "manage_bundles",
    description: "&Manage Resources...",
    tooltip: "Manage Resources",
  },
  {
    id: "rotateimage",
    description: "&Rotate Image...",
    tooltip: "Rotate Image",
  },
  {
    id: "rotateImageCW90",
    description: "Rotate &Image 90° to the Right",
    tooltip: "Rotate Image 90° to the Right",
  },
  {
    id: "rotateImage180",
    description: "Rotate Image &180°",
    tooltip: "Rotate Image 180°",
  },
  {
    id: "rotateImageCCW90",
    description: "Rotate Image &90° to the Left",
    tooltip: "Rotate Image 90° to the Left",
  },
  {
    id: "mirrorImageHorizontal",
    description: "&Mirror Image Horizontally",
    tooltip: "Mirror Image Horizontally",
  },
  {
    id: "mirrorImageVertical",
    description: "Mirror Image &Vertically",
    tooltip: "Mirror Image Vertically",
  },
  {
    id: "rotatelayer",
    description: "&Rotate Layer...",
    tooltip: "Rotate Layer",
  },
  {
    id: "rotateLayer180",
    description: "Rotate Layer &180°",
    tooltip: "Rotate Layer 180°",
  },
  {
    id: "rotateLayerCW90",
    description: "Rotate &Layer 90° to the Right",
    tooltip: "Rotate Layer 90° to the Right",
  },
  {
    id: "rotateLayerCCW90",
    description: "Rotate Layer &90° to the Left",
    tooltip: "Rotate Layer 90° to the Left",
  },
  {
    id: "rotateAllLayers",
    description: "&Rotate All Layers...",
    tooltip: "Rotate All Layers",
  },
  {
    id: "rotateAllLayersCW90",
    description: "Rotate All &Layers 90° to the Right",
    tooltip: "Rotate All Layers 90° to the Right",
  },
  {
    id: "rotateAllLayersCCW90",
    description: "Rotate All Layers &90° to the Left",
    tooltip: "Rotate All Layers 90° to the Left",
  },
  {
    id: "rotateAllLayers180",
    description: "Rotate All Layers &180°",
    tooltip: "Rotate All Layers 180°",
  },
  {
    id: "separate",
    description: "Separate Ima&ge...",
    tooltip: "Separate Image",
  },
  {
    id: "shearimage",
    description: "&Shear Image...",
    tooltip: "Shear Image",
  },
  {
    id: "shearlayer",
    description: "&Shear Layer...",
    tooltip: "Shear Layer",
  },
  {
    id: "shearAllLayers",
    description: "&Shear All Layers...",
    tooltip: "Shear All Layers",
  },
  {
    id: "waveletdecompose",
    description: "&Wavelet Decompose ...",
    tooltip: "Wavelet Decompose",
  },
  {
    id: "insert_column_left",
    description: "Insert Column Left",
    tooltip:
      "Insert column to the left of selection, moving the tail of animation to the right",
  },
  {
    id: "insert_column_right",
    description: "Insert Column Right",
    tooltip:
      "Insert column to the right of selection, moving the tail of animation to the right",
  },
  {
    id: "insert_multiple_columns",
    description: "Insert Multiple Columns",
    tooltip: "Insert several columns based on user parameters.",
  },
  {
    id: "remove_columns_and_pull",
    description: "Remove Column and Pull",
    tooltip: "Remove columns moving the tail of animation to the left",
  },
  {
    id: "remove_columns",
    description: "Remove Column",
    tooltip: "Remove columns without moving anything around",
  },
  {
    id: "insert_hold_column",
    description: "Insert Hold Column",
    tooltip: "Insert a hold column into the frame at the current position",
  },
  {
    id: "insert_multiple_hold_columns",
    description: "Insert Multiple Hold Columns",
    tooltip: "Insert N hold columns into the frame at the current position",
  },
  {
    id: "remove_hold_column",
    description: "Remove Hold Column",
    tooltip: "Remove a hold column from the frame at the current position",
  },
  {
    id: "remove_multiple_hold_columns",
    description: "Remove Multiple Hold Columns",
    tooltip: "Remove N hold columns from the frame at the current position",
  },
  {
    id: "mirror_columns",
    description: "Mirror Columns",
    tooltip: "Mirror columns' position",
  },
  {
    id: "copy_columns_to_clipboard",
    description: "Copy Columns to Clipboard",
    tooltip: "Copy columns to clipboard",
  },
  {
    id: "cut_columns_to_clipboard",
    description: "Cut Columns to Clipboard",
    tooltip: "Cut columns to clipboard",
  },
  {
    id: "paste_columns_from_clipboard",
    description: "Paste Columns from Clipboard",
    tooltip: "Paste columns from clipboard",
  },
  {
    id: "add_blank_frame",
    description: "Create Blank Frame",
    tooltip: "Add blank frame",
  },
  {
    id: "add_duplicate_frame",
    description: "Create Duplicate Frame",
    tooltip: "Add duplicate frame",
  },
  {
    id: "insert_keyframe_left",
    description: "Insert Keyframe Left",
    tooltip:
      "Insert keyframes to the left of selection, moving the tail of animation to the right.",
  },
  {
    id: "insert_keyframe_right",
    description: "Insert Keyframe Right",
    tooltip:
      "Insert keyframes to the right of selection, moving the tail of animation to the right.",
  },
  {
    id: "insert_multiple_keyframes",
    description: "Insert Multiple Keyframes",
    tooltip: "Insert several keyframes based on user parameters.",
  },
  {
    id: "remove_frames_and_pull",
    description: "Remove Frame and Pull",
    tooltip: "Remove keyframes moving the tail of animation to the left",
  },
  {
    id: "remove_frames",
    description: "Remove Keyframe",
    tooltip: "Remove keyframes without moving anything around",
  },
  {
    id: "insert_hold_frame",
    description: "Insert Hold Frame",
    tooltip: "Insert a hold frame after every keyframe",
  },
  {
    id: "insert_multiple_hold_frames",
    description: "Insert Multiple Hold Frames",
    tooltip: "Insert N hold frames after every keyframe",
  },
  {
    id: "remove_hold_frame",
    description: "Remove Hold Frame",
    tooltip: "Remove a hold frame after every keyframe",
  },
  {
    id: "remove_multiple_hold_frames",
    description: "Remove Multiple Hold Frames",
    tooltip: "Remove N hold frames after every keyframe",
  },
  {
    id: "mirror_frames",
    description: "Mirror Frames",
    tooltip: "Mirror frames' position",
  },
  {
    id: "copy_frames_to_clipboard",
    description: "Copy to Clipboard",
    tooltip: "Copy frames to clipboard",
  },
  {
    id: "cut_frames_to_clipboard",
    description: "Cut to Clipboard",
    tooltip: "Cut frames to clipboard",
  },
  {
    id: "paste_frames_from_clipboard",
    description: "Paste from Clipboard",
    tooltip: "Paste frames from clipboard",
  },
  {
    id: "set_start_time",
    description: "Set Start Time",
    tooltip: "Set Start Time",
  },
  {
    id: "set_end_time",
    description: "Set End Time",
    tooltip: "Set End Time",
  },
  {
    id: "update_playback_range",
    description: "Update Playback Range",
    tooltip: "Update Playback Range",
  },
  {
    id: "toggle_playback",
    description: "Play / pause animation",
    tooltip: "Play / pause animation",
  },
  {
    id: "stop_playback",
    description: "Stop animation",
    tooltip: "Stop animation",
  },
  {
    id: "previous_frame",
    description: "Previous frame",
    tooltip: "Move to previous frame",
  },
  {
    id: "next_frame",
    description: "Next frame",
    tooltip: "Move to next frame",
  },
  {
    id: "auto_key",
    description: "Auto Frame Mode",
    tooltip: "Auto Frame Mode",
  },
  {
    id: "drop_frames",
    description: "Drop FramesEnable to preserve playback timing.",
    tooltip: "Drop FramesEnable to preserve playback timing.",
  },
  {
    id: "show-global-selection-mask",
    description: "&Show Global Selection Mask",
    tooltip:
      "Shows global selection as a usual selection mask in <b>Layers</b> docker",
  },
  {
    id: "RenameCurrentLayer",
    description: "Rename current layer",
    tooltip: "Rename current layer",
  },
  {
    id: "layer_properties",
    description: "&Properties...",
    tooltip: "Properties",
  },
  {
    id: "remove_layer",
    description: "&Remove Layer",
    tooltip: "Remove Layer",
  },
  {
    id: "move_layer_up",
    description: "Move Layer or Mask Up",
    tooltip: "Move Layer or Mask Up",
  },
  {
    id: "move_layer_down",
    description: "Move Layer or Mask Down",
    tooltip: "Move Layer or Mask Down",
  },
  {
    id: "set-copy-from",
    description: "Set Copy F&rom...",
    tooltip: "Set the source for the selected clone layer(s).",
  },
  {
    id: "toggle_onion_skin",
    description: "Toggle onion skin",
    tooltip: "Toggle onion skin",
  },
  {
    id: "create_snapshot",
    description: "Create Snapshot",
    tooltip: "Create Snapshot",
  },
  {
    id: "switchto_snapshot",
    description: "Switch to Selected Snapshot",
    tooltip: "Switch to selected snapshot",
  },
  {
    id: "remove_snapshot",
    description: "Remove Selected Snapshot",
    tooltip: "Remove Selected Snapshot",
  },
  {
    id: "increase_brush_size",
    description: "Increase Brush Size",
    tooltip: "Increase Brush Size",
  },
  {
    id: "decrease_brush_size",
    description: "Decrease Brush Size",
    tooltip: "Decrease Brush Size",
  },
  {
    id: "selection_tool_mode_add",
    description: "Selection Mode: Add",
    tooltip: "Selection Mode: Add",
  },
  {
    id: "selection_tool_mode_replace",
    description: "Selection Mode: Replace",
    tooltip: "Selection Mode: Replace",
  },
  {
    id: "selection_tool_mode_subtract",
    description: "Selection Mode: Subtract",
    tooltip: "Selection Mode: Subtract",
  },
  {
    id: "selection_tool_mode_intersect",
    description: "Selection Mode: Intersect",
    tooltip: "Selection Mode: Intersect",
  },
  {
    id: "undo_polygon_selection",
    description: "Undo Polygon Selection Points",
    tooltip: "Undo Polygon Selection Points",
  },
  {
    id: "object_order_front",
    description: "Bring to &Front",
    tooltip: "Bring to Front",
  },
  {
    id: "object_order_raise",
    description: "&Raise",
    tooltip: "Raise",
  },
  {
    id: "object_order_lower",
    description: "&Lower",
    tooltip: "Lower",
  },
  {
    id: "object_order_back",
    description: "Send to &Back",
    tooltip: "Send to Back",
  },
  {
    id: "object_transform_rotate_90_cw",
    description: "Rotate 90° CW",
    tooltip: "Rotate object 90° clockwise",
  },
  {
    id: "object_transform_rotate_90_ccw",
    description: "Rotate 90° CCW",
    tooltip: "Rotate object 90° counterclockwise",
  },
  {
    id: "object_transform_rotate_180",
    description: "Rotate 180°",
    tooltip: "Rotate object 180°",
  },
  {
    id: "object_transform_mirror_horizontally",
    description: "Mirror Horizontally",
    tooltip: "Mirror object horizontally",
  },
  {
    id: "object_transform_mirror_vertically",
    description: "Mirror Vertically",
    tooltip: "Mirror object vertically",
  },
  {
    id: "object_transform_reset",
    description: "Reset Transformations",
    tooltip: "Reset object transformations",
  },
  {
    id: "set_no_brush_smoothing",
    description: "Brush Smoothing: Disabled",
    tooltip: "Brush Smoothing: Disabled",
  },
  {
    id: "set_simple_brush_smoothing",
    description: "Brush Smoothing: Basic",
    tooltip: "Brush Smoothing: Basic",
  },
  {
    id: "set_weighted_brush_smoothing",
    description: "Brush Smoothing: Weighted",
    tooltip: "Brush Smoothing: Weighted",
  },
  {
    id: "set_stabilizer_brush_smoothing",
    description: "Brush Smoothing: Stabilizer",
    tooltip: "Brush Smoothing: Stabilizer",
  },
  {
    id: "toggle_assistant",
    description: "Toggle Snap To Assistants",
    tooltip: "Toggle Snap to Assistants",
  },
  {
    id: "pathpoint-corner",
    description: "Corner point",
    tooltip: "Corner point",
  },
  {
    id: "pathpoint-smooth",
    description: "Smooth point",
    tooltip: "Smooth point",
  },
  {
    id: "pathpoint-symmetric",
    description: "Symmetric Point",
    tooltip: "Symmetric Point",
  },
  {
    id: "pathpoint-curve",
    description: "Make curve point",
    tooltip: "Make curve point",
  },
  {
    id: "pathpoint-line",
    description: "Make line point",
    tooltip: "Make line point",
  },
  {
    id: "pathsegment-line",
    description: "Segment to Line",
    tooltip: "Segment to Line",
  },
  {
    id: "pathsegment-curve",
    description: "Segment to Curve",
    tooltip: "Segment to Curve",
  },
  {
    id: "pathpoint-insert",
    description: "Insert point",
    tooltip: "Insert point",
  },
  {
    id: "pathpoint-remove",
    description: "Remove point",
    tooltip: "Remove point",
  },
  {
    id: "path-break-point",
    description: "Break at point",
    tooltip: "Break at point",
  },
  {
    id: "path-break-segment",
    description: "Break at segment",
    tooltip: "Break at segment",
  },
  {
    id: "pathpoint-join",
    description: "Join with segment",
    tooltip: "Join with segment",
  },
  {
    id: "pathpoint-merge",
    description: "Merge points",
    tooltip: "Merge points",
  },
  {
    id: "convert-to-path",
    description: "To Path",
    tooltip: "To Path",
  },
  {
    id: "calligraphy_increase_width",
    description: "Calligraphy: increase width",
    tooltip: "Calligraphy: increase width",
  },
  {
    id: "calligraphy_decrease_width",
    description: "Calligraphy: decrease width",
    tooltip: "Calligraphy: decrease width",
  },
  {
    id: "calligraphy_increase_angle",
    description: "Calligraphy: increase angle",
    tooltip: "Calligraphy: increase angle",
  },
  {
    id: "calligraphy_decrease_angle",
    description: "Calligraphy: decrease angle",
    tooltip: "Calligraphy: decrease angle",
  },
  {
    id: "movetool-move-up",
    description: "Move up",
    tooltip: "Move up",
  },
  {
    id: "movetool-move-down",
    description: "Move down",
    tooltip: "Move down",
  },
  {
    id: "movetool-move-left",
    description: "Move left",
    tooltip: "Move left",
  },
  {
    id: "movetool-move-right",
    description: "Move right",
    tooltip: "Move right",
  },
  {
    id: "movetool-move-up-more",
    description: "Move up more",
    tooltip: "Move up more",
  },
  {
    id: "movetool-move-down-more",
    description: "Move down more",
    tooltip: "Move down more",
  },
  {
    id: "movetool-move-left-more",
    description: "Move left more",
    tooltip: "Move left more",
  },
  {
    id: "movetool-move-right-more",
    description: "Move right more",
    tooltip: "Move right more",
  },
  {
    id: "object_align_horizontal_left",
    description: "Align Left",
    tooltip: "Align Left",
  },
  {
    id: "object_align_horizontal_center",
    description: "Horizontally Center",
    tooltip: "Horizontally Center",
  },
  {
    id: "object_align_horizontal_right",
    description: "Align Right",
    tooltip: "Align Right",
  },
  {
    id: "object_align_vertical_top",
    description: "Align Top",
    tooltip: "Align Top",
  },
  {
    id: "object_align_vertical_center",
    description: "Vertically Center",
    tooltip: "Vertically Center",
  },
  {
    id: "object_align_vertical_bottom",
    description: "Align Bottom",
    tooltip: "Align Bottom",
  },
  {
    id: "object_distribute_horizontal_left",
    description: "Distribute Left",
    tooltip: "Distribute left edges equidistantly",
  },
  {
    id: "object_distribute_horizontal_center",
    description: "Distribute Centers Horizontally",
    tooltip: "Distribute centers equidistantly horizontally",
  },
  {
    id: "object_distribute_horizontal_right",
    description: "Distribute Right",
    tooltip: "Distribute right edges equidistantly",
  },
  {
    id: "object_distribute_horizontal_gaps",
    description: "Distribute Horizontal Gap",
    tooltip: "Make horizontal gaps between objects equal",
  },
  {
    id: "object_distribute_vertical_top",
    description: "Distribute Top",
    tooltip: "Distribute top edges equidistantly",
  },
  {
    id: "object_distribute_vertical_center",
    description: "Distribute Centers Vertically",
    tooltip: "Distribute centers equidistantly vertically",
  },
  {
    id: "object_distribute_vertical_bottom",
    description: "Distribute Bottom",
    tooltip: "Distribute bottom edges equidistantly",
  },
  {
    id: "object_distribute_vertical_gaps",
    description: "Distribute Vertical Gap",
    tooltip: "Make vertical gaps between objects equal",
  },
  {
    id: "object_group",
    description: "Group",
    tooltip: "Group",
  },
  {
    id: "object_ungroup",
    description: "Ungroup",
    tooltip: "Ungroup",
  },
  {
    id: "object_unite",
    description: "Unite",
    tooltip: "Create boolean union of multiple objects",
  },
  {
    id: "object_intersect",
    description: "Intersect",
    tooltip: "Create boolean intersection of multiple objects",
  },
  {
    id: "object_subtract",
    description: "Subtract",
    tooltip: "Subtract multiple objects from the first selected one",
  },
  {
    id: "object_split",
    description: "Split",
    tooltip: "Split objects with multiple subpaths into multiple objects",
  },
  {
    id: "movetool-show-coordinates",
    description: "Show Coordinates",
    tooltip: "Show absolute coordinates and offset while move action",
  },
  {
    id: "file_new",
    description: "&New...",
    tooltip: "Create new document",
  },
  {
    id: "file_open",
    description: "&Open...",
    tooltip: "Open an existing document",
  },
  {
    id: "file_quit",
    description: "&Quit",
    tooltip: "Quit application",
  },
  {
    id: "options_configure_toolbars",
    description: "Configure Tool&bars...",
    tooltip: "Configure Toolbars",
  },
  {
    id: "fullscreen",
    description: "F&ull Screen Mode",
    tooltip: "Display the window in full screen",
  },
  {
    id: "file_open_recent",
    description: "Open &Recent",
    tooltip: "Open a document which was recently opened",
  },
  {
    id: "file_save",
    description: "&Save",
    tooltip: "Save",
  },
  {
    id: "file_save_as",
    description: "Save &As...",
    tooltip: "Save document under a new name",
  },
  {
    id: "edit_undo",
    description: "&Undo",
    tooltip: "Undo last action",
  },
  {
    id: "edit_redo",
    description: "&Redo",
    tooltip: "Redo last undone action",
  },
  {
    id: "file_import_animation",
    description: "I&mport animation frames...",
    tooltip: "Import animation frames",
  },
  {
    id: "file_close_all",
    description: "&Close All",
    tooltip: "Close All",
  },
  {
    id: "file_import_file",
    description: "Open ex&isting Document as Untitled Document...",
    tooltip: "Open existing Document as Untitled Document",
  },
  {
    id: "file_export_file",
    description: "E&xport...",
    tooltip: "Export",
  },
  {
    id: "file_documentinfo",
    description: "&Document Information",
    tooltip: "Document Information",
  },
  {
    id: "theme_menu",
    description: "&Themes",
    tooltip: "Themes",
  },
  {
    id: "view_toggledockers",
    description: "&Show Dockers",
    tooltip: "Show Dockers",
  },
  {
    id: "reset_configurations",
    description: "&Reset Krita Configurations",
    tooltip: "Reset Krita Configurations",
  },
  {
    id: "view_detached_canvas",
    description: "&Detach canvas",
    tooltip: "Show the canvas on a separate window",
  },
  {
    id: "settings_dockers_menu",
    description: "&Dockers",
    tooltip: "Dockers",
  },
  {
    id: "window",
    description: "&Window",
    tooltip: "Window",
  },
  {
    id: "style_menu",
    description: "St&yles",
    tooltip: "Styles",
  },
  {
    id: "windows_cascade",
    description: "C&ascade",
    tooltip: "Cascade",
  },
  {
    id: "windows_tile",
    description: "&Tile",
    tooltip: "Tile",
  },
  {
    id: "windows_next",
    description: "N&ext",
    tooltip: "Next",
  },
  {
    id: "windows_previous",
    description: "&Previous",
    tooltip: "Previous",
  },
  {
    id: "view_newwindow",
    description: "&New Window",
    tooltip: "New Window",
  },
  {
    id: "file_close",
    description: "C&lose",
    tooltip: "Close",
  },
  {
    id: "file_sessions",
    description: "S&essions...",
    tooltip: "Open session manager",
  },
  {
    id: "options_configure",
    description: "&Configure Krita...",
    tooltip: "Configure Krita",
  },
  {
    id: "expanding_spacer_0",
    description: "Expanding Spacer",
    tooltip: "Expanding Spacer",
  },
  {
    id: "expanding_spacer_1",
    description: "Expanding Spacer",
    tooltip: "Expanding Spacer",
  },
  {
    id: "help_contents",
    description: "Krita &Handbook",
    tooltip: "Krita Handbook",
  },
  {
    id: "help_report_bug",
    description: "&Report Bug...",
    tooltip: "Report Bug",
  },
  {
    id: "switch_application_language",
    description: "Switch Application &Language...",
    tooltip: "Switch Application Language",
  },
  {
    id: "help_about_app",
    description: "&About Krita",
    tooltip: "About Krita",
  },
  {
    id: "help_about_kde",
    description: "About &KDE",
    tooltip: "About KDE",
  },
  {
    id: "color_space",
    description: "&Color Space",
    tooltip: "Plugin to change color space of selected documents.",
  },
  {
    id: "document_tools",
    description: "&Document Tools",
    tooltip: "Plugin to manipulate properties of selected documents.",
  },
  {
    id: "export_layers",
    description: "&Export Layers",
    tooltip: "Plugin to export layers from a document.",
  },
  {
    id: "filter_manager",
    description: "&Filter Manager",
    tooltip: "Plugin to filters management.",
  },
  {
    id: "plugin_importer",
    description: "&Import Python Plugin...",
    tooltip: "Import Python Plugin",
  },
  {
    id: "python_scripter",
    description: "&Scripter",
    tooltip: "Scripter",
  },
  {
    id: "ten_brushes",
    description: "&Ten Brushes",
    tooltip: "Assign ten brush presets to ten shortcuts.",
  },
  {
    id: "activate_preset_1",
    description: "Activate Brush Preset 1",
    tooltip: "Activate Brush Preset 1",
  },
  {
    id: "activate_preset_2",
    description: "Activate Brush Preset 2",
    tooltip: "Activate Brush Preset 2",
  },
  {
    id: "activate_preset_3",
    description: "Activate Brush Preset 3",
    tooltip: "Activate Brush Preset 3",
  },
  {
    id: "activate_preset_4",
    description: "Activate Brush Preset 4",
    tooltip: "Activate Brush Preset 4",
  },
  {
    id: "activate_preset_5",
    description: "Activate Brush Preset 5",
    tooltip: "Activate Brush Preset 5",
  },
  {
    id: "activate_preset_6",
    description: "Activate Brush Preset 6",
    tooltip: "Activate Brush Preset 6",
  },
  {
    id: "activate_preset_7",
    description: "Activate Brush Preset 7",
    tooltip: "Activate Brush Preset 7",
  },
  {
    id: "activate_preset_8",
    description: "Activate Brush Preset 8",
    tooltip: "Activate Brush Preset 8",
  },
  {
    id: "activate_preset_9",
    description: "Activate Brush Preset 9",
    tooltip: "Activate Brush Preset 9",
  },
  {
    id: "activate_preset_0",
    description: "Activate Brush Preset 0",
    tooltip: "Activate Brush Preset 0",
  },
  {
    id: "ten_scripts",
    description: "Te&n Scripts",
    tooltip: "Assign ten scripts to ten shortcuts.",
  },
  {
    id: "execute_script_1",
    description: "Execute Script 1",
    tooltip: "Execute Script 1",
  },
  {
    id: "execute_script_2",
    description: "Execute Script 2",
    tooltip: "Execute Script 2",
  },
  {
    id: "execute_script_3",
    description: "Execute Script 3",
    tooltip: "Execute Script 3",
  },
  {
    id: "execute_script_4",
    description: "Execute Script 4",
    tooltip: "Execute Script 4",
  },
  {
    id: "execute_script_5",
    description: "Execute Script 5",
    tooltip: "Execute Script 5",
  },
  {
    id: "execute_script_6",
    description: "Execute Script 6",
    tooltip: "Execute Script 6",
  },
  {
    id: "execute_script_7",
    description: "Execute Script 7",
    tooltip: "Execute Script 7",
  },
  {
    id: "execute_script_8",
    description: "Execute Script 8",
    tooltip: "Execute Script 8",
  },
  {
    id: "execute_script_9",
    description: "Execute Script 9",
    tooltip: "Execute Script 9",
  },
  {
    id: "execute_script_10",
    description: "Execute Script 10",
    tooltip: "Execute Script 10",
  },
  {
    id: "mainToolBar",
    description: "Hide File Toolbar",
    tooltip: "Hide File Toolbar",
  },
  {
    id: "BrushesAndStuff",
    description: "Hide Brushes and Stuff Toolbar",
    tooltip: "Hide Brushes and Stuff Toolbar",
  },
];

export default AvailableActions;
// export default Information;
